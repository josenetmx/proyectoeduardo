<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		/*
                $users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
                */
                $acceso = Acceso::model()->findByAttributes(
                        array(
                            'usuario'=>  $this->username,
                        ));
                        //var_dump($acceso);
                //exit;
                if(empty($acceso) || $acceso->contrasenia != md5($this->password) || !$acceso->activo){
                    $this->errorCode=self::ERROR_USERNAME_INVALID;
                }
                /*
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
                */
		else{
			$this->setState('id_usuario', $acceso->id);
                        $this->errorCode=self::ERROR_NONE;
                }
                //print_r($this);
                //exit;
		return !$this->errorCode;
	}
}