<?php

/**
 * This is the model class for table "inventario".
 *
 * The followings are the available columns in table 'inventario':
 * @property integer $id
 * @property string $sku
 * @property string $codigoInterno
 * @property string $descripcion
 * @property integer $ingresaPor
 * @property integer $cantidad
 * @property integer $presentacion
 * @property string $fechaDeCreacion
 * @property string $fechaDeModificacion
 * @property integer $activo
 */
class Inventario extends CActiveRecord
{
        public $_presentacion;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, ingresaPor, cantidad, presentacion,fechaDeCreacion, fechaDeModificacion', 'required'),
			array('ingresaPor, cantidad, presentacion, activo', 'numerical', 'integerOnly'=>true),
			array('sku, codigoInterno', 'length', 'max'=>60),
			array('descripcion', 'length', 'max'=>120),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sku, codigoInterno, descripcion, ingresaPor, cantidad, presentacion, fechaDeCreacion, fechaDeModificacion, activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sku' => 'Sku',
			'codigoInterno' => 'Codigo Interno',
			'descripcion' => 'Descripcion',
			'ingresaPor' => 'Ingresa Por',
			'cantidad' => 'Cantidad',
			'presentacion' => 'Presentacion',
			'fechaDeCreacion' => 'Fecha De Creacion',
			'fechaDeModificacion' => 'Fecha De Modificacion',
			'activo' => 'Activo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sku',$this->sku,true);
		$criteria->compare('codigoInterno',$this->codigoInterno,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('ingresaPor',$this->ingresaPor);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('presentacion',$this->presentacion);
		$criteria->compare('fechaDeCreacion',$this->fechaDeCreacion,true);
		$criteria->compare('fechaDeModificacion',$this->fechaDeModificacion,true);
		$criteria->compare('activo',$this->activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Inventario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
