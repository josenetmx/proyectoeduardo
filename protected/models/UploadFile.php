<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class UploadFile extends CFormModel
{
	public $nombre;
	public $archivo;
        public $isNewRecord;
	//public $verifyCode;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('nombre', 'required'),
			// email has to be a valid email address
			//array('email', 'email'),
			// verifyCode needs to be entered correctly
			//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
                        array('archivo', 'file', 
                                            'types'=>'csv',
                                            'maxSize'=>1024 * 1024 * 20, // 10MB
                                            'tooLarge'=>'The file was larger than 10MB. Please upload a smaller file.',
                                            'allowEmpty' => true,
                                            'wrongType'=>'Solo CSV permitido'
                            ),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
                
		return array(
			//'verifyCode'=>'Verification Code',
                    'archivo'=>'Sube un archivo csv',
		);
             
	}
}