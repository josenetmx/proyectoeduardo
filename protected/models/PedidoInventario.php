<?php

/**
 * This is the model class for table "pedidoInventario".
 *
 * The followings are the available columns in table 'pedidoInventario':
 * @property integer $id
 * @property integer $fk_id_ped
 * @property integer $fk_id_inv
 * @property integer $cantidad
 * @property string $comentarios
 * @property integer $activo
 * @property integer $aprovada
 * @property string $fechaDeCreacion
 */
class PedidoInventario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pedidoInventario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fk_id_ped, fk_id_inv, cantidad', 'required'),
			array('fk_id_ped, fk_id_inv, cantidad, activo, aprovada', 'numerical', 'integerOnly'=>true),
			array('comentarios, fechaDeCreacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fk_id_ped, fk_id_inv, cantidad, comentarios, activo, aprovada, fechaDeCreacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fk_id_ped' => 'Fk Id Ped',
			'fk_id_inv' => 'Fk Id Inv',
			'cantidad' => 'Cantidad',
			'comentarios' => 'Comentarios',
			'activo' => 'Activo',
			'aprovada' => 'Aprovada',
			'fechaDeCreacion' => 'Fecha De Creacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fk_id_ped',$this->fk_id_ped);
		$criteria->compare('fk_id_inv',$this->fk_id_inv);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('comentarios',$this->comentarios,true);
		$criteria->compare('activo',$this->activo);
		$criteria->compare('aprovada',$this->aprovada);
		$criteria->compare('fechaDeCreacion',$this->fechaDeCreacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PedidoInventario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
