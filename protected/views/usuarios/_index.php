<?php
//$arr1 = usuario
//$arr2 = ocupaciones
?>

<div class="view">
    <b><?php echo CHtml::encode($usuario->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($usuario->id), array('view', 'id' => $usuario->id)); ?>
    <br />

    <b><?php echo CHtml::encode($usuario->getAttributeLabel('nombre')); ?>:</b>
    <?php echo CHtml::encode($usuario->nombre); ?>
    <br />
    <?php if (!is_null($usuario->apellidos)) {
        ?>    

        <b><?php echo CHtml::encode($usuario->getAttributeLabel('apellidos')); ?>:</b>
        <?php echo CHtml::encode($usuario->apellidos); ?>
        <br />
    <?php } ?>

    <?php if (!is_null($usuario->correo)) { ?>
        <b><?php echo CHtml::encode($usuario->getAttributeLabel('correo')); ?>:</b>
        <?php echo CHtml::encode($usuario->correo); ?>
        <br />
    <?php } ?>
    <b><?php echo CHtml::encode($usuario->getAttributeLabel('fechaDeCreacion')); ?>:</b>
    <?php echo CHtml::encode($usuario->fechaDeCreacion); ?>
    <br />

    <?php if (count($ocupacion) > 0) { ?>
        <b>Ocupaciones:</b>
        <?php
        foreach ($ocupacion as $value) {
            ?>  
            <?php echo CHtml::encode($value['ocupacion']); ?>
            <br />
        <?php
        }
    }else{
    ?>
       <b>Ocupaciones:</b>
       Pendiente por asignar
       <br />
    <?php }?>        
</div>
