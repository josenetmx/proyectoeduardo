<?php
/* @var $this UsuariosController */
/* @var $model Usuarios */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'usuarios-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textArea($model,'nombre',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellidos'); ?>
		<?php echo $form->textArea($model,'apellidos',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'apellidos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correo'); ?>
		<?php echo $form->textArea($model,'correo',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

        <div class="row">
            <span><h2>Ocupaciones</h2></span>
            
            <div class="row">
                    <?php echo $form->checkBoxList($model,'_ocupaciones',$ocupaciones); ?>
                    <?php echo $form->error($model,'_ocupaciones'); ?>
            </div>
            <?php 
            /*$ocupaciones = Ocupacion::model();
            //
            $usOcCh = UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us and activo',array(':fk_id_us'=>$model->id));
            $arr = array();
            foreach ($usOcCh as $value) {
                array_push($arr, $value['fk_id_oc']);
            }
            //print_r($arr);
            $ocupaciones->selectedID = $arr;
            //foreach ($ocupaciones as $key => $value) {
                //print_r($value->id);
                echo CHtml::activeCheckBoxList($ocupaciones, 'selectedID', CHtml::listData(
                                $ocupaciones->findAll('activo'), 'id', 'ocupacion'
                ));*/ /* In this case I want to show only the sucursales-departments that belong 
                 * to the company that the Employee belongs to. Otherwise you just 
                 * Sucursal::model()->findAll() without condition */
                ?>            

            <?php //} ?>
            
            <?php /*
            $usOcCh = UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us',array(':fk_id_us'=>$model->id));
            //print_r($usOcCh);
            $ocupaciones = Ocupacion::model()->findAll();
            //print_r($ocupaciones);
            foreach ($ocupaciones as $value) {
                echo $form->checkBoxList($ocupaciones,'id',$value['ocupacion']);
            }
            */
            ?>
        </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->