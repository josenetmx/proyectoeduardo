<?php
/* @var $this UsuariosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Usuarios',
);

$this->menu=array(
	array('label'=>'Create Usuarios', 'url'=>array('create')),
	array('label'=>'Manage Usuarios', 'url'=>array('admin')),
);
?>

<h1>Usuarios</h1>
<div class="view">
<?php /*$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 
*/
//$nombres_ocupaciones= Ocupacion::model()->findAll();
//$nombres_ocupaciones_array= array();
//foreach($nombres_ocupaciones as $nombres_ocupacion){
//    $nombres_ocupaciones_array[$nombres_ocupacion->id]=$nombres_ocupacion->ocupacion;
//}
$arrOcupaciones = array();
foreach ($usuarios as $usuario){
    //print_r($usuario);
    $ocupaciones= UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us and activo', array(':fk_id_us'=>$usuario->id));
    //echo "<br />";
    //echo "<br />";
    foreach($ocupaciones as $ocupacion){
        array_push($arrOcupaciones, array('ocupacion'=>$nombres_ocupaciones_array[$ocupacion->fk_id_oc]));
    }
    //echo "<hr />";
    $this->renderPartial('_index', array('usuario'=>$usuario,'ocupacion'=>$arrOcupaciones));    
        $arrOcupaciones = array();
}
?>

<div id="data">
   <?php $this->renderPartial('_confirm', array('message'=>$data)); ?>
</div>
    <?php
    echo CHtml::beginForm();
    ?>
    
<div class="row">
    <?php
    //print_r($nombres_ocupaciones_array);
    
    echo CHtml::dropDownList('_ocupacion','',$nombres_ocupaciones_array, array(
        'ajax' => array(
            'type' => 'POST',
            'url' => CController::createUrl('usuarios/RefreshDiv'), //url to call.
            //Style: CController::createUrl('currentController/methodToCall')
            //'data'=>array('Ocupacion'=>'js:this.options[this.selectedIndex].text'),
            'update' => '#seleccion',

    )));
    ?>
</div>
    <?php
    echo CHtml::endForm();
    ?>
<div id="seleccion">
    <p>Sin seleccionar</p>
</div>    
<?php echo CHtml::ajaxButton ("Ejemplo ajax",
                              CController::createUrl('usuarios/CheckUser'), 
                              array('update' => '#data'));
?>    
    
</div>
