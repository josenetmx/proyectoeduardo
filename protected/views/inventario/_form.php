<?php
/* @var $this InventarioController */
/* @var $model Inventario */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventario-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'sku'); ?>
		<?php echo $form->textField($model,'sku',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'sku'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codigoInterno'); ?>
		<?php echo $form->textField($model,'codigoInterno',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'codigoInterno'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textField($model,'descripcion',array('size'=>60,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>
        
	<div class="row">
		<?php echo (($model->isNewRecord) ? $form->labelEx($model,'ingresaPor',array('style'=>'display:none;')):$form->labelEx($model,'ingresaPor')); ?>
                
		<?php echo (($model->isNewRecord) ? $form->textField($model,'ingresaPor',array('value'=>Yii::app()->user->id_usuario,'style'=>'display:none;')):$form->textField($model,'ingresaPor')); ?>
		<?php echo $form->error($model,'ingresaPor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'presentacion'); ?>
		<?php echo $form->dropDownList($model,'presentacion',$presentacion); ?>
		<?php echo $form->error($model,'_presentacion'); ?>
	</div>
        <?php if(!$model->isNewRecord){?>
	<div class="row">
		<?php echo $form->labelEx($model,'fechaDeCreacion'); ?>
		<?php echo $form->textField($model,'fechaDeCreacion'); ?>
		<?php echo $form->error($model,'fechaDeCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaDeModificacion'); ?>
		<?php echo $form->textField($model,'fechaDeModificacion'); ?>
		<?php echo $form->error($model,'fechaDeModificacion'); ?>
	</div>
        <?php }?>
	<div class="row">
		<?php echo (($model->isNewRecord) ? $form->labelEx($model,'activo',array('style'=>'display:none;')):$form->labelEx($model,'activo')); ?>
		<?php echo (($model->isNewRecord) ? $form->textField($model,'activo',array('style'=>'display:none;')):$form->textField($model,'activo')); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->