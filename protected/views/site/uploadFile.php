<div class="form">

    <?php
        /*$form = $this->beginWidget('CActiveForm',
                array(
                    'id'=>'upload-form',
                    'method'=>'POST',
                    'action'=>Yii::app()->createUrl('/site/upload'),
                    'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    //'enableClientValidation'=>true,
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                    ),
                    
                ));
    ?>
    <div class="row">
        <?php 
        echo $form->labelEx($model,'nombre');
        echo $form->textField($model,'nombre');
        echo $form->error($model,'nombre',array('class'=>'text-error'));
        ?>
        
    </div>
    
    <div class="row">
       <?php 
       $form->widget('CMultiFileUpload',
               array(
                      'model'=>$model,
                      'name' =>'archivo',
                      'attribute'=> 'archivo',
                      'accept'=> 'csv',
                      'denied'=> 'El tipo de archivo no es de los permitidos',
                      'max' => 1,
                      'duplicate'=> 'archivo duplicado',
       ));
       
       echo $form->error($model,'archivo');
       ?>
    </div>
    <div class="row">
        <?php echo CHtml::submitButton('Subir archivo');?>
    </div>
    <?php $this->endWidget();?>
</div>
*/ 
    $form=$this->beginWidget('CActiveForm', array(
	'id'=>'uploadfile-form',
        
	/*'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),*/
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    
        ));
    
    ?>

<div class="row">
        <?php 
        echo $form->labelEx($model,'nombre');
        echo $form->textField($model,'nombre');
        echo $form->error($model,'nombre',array('class'=>'text-error'));
        ?>
        
    </div>
    
    <div class="row">
       <?php 
       
       /*
       $form->widget('CMultiFileUpload',
               array(
                      'model'=>$model,
                      'name' =>'archivo',
                      'attribute'=> 'archivo',
                      'accept'=> 'csv',
                      'denied'=> 'El tipo de archivo no es de los permitidos',
                      'max' => 1,
                      'duplicate'=> 'archivo duplicado',
       ));
       
       echo $form->error($model,'archivo');
        * 
        */
       echo $form->labelEx($model,'archivo');
        echo $form->fileField($model,'archivo');
        echo $form->error($model,'archivo',array('class'=>'text-error'));
       ?>
    </div>
    <div class="row">
         <?php echo CHtml::submitButton('Guardar'); ?>
    </div>
    <?php $this->endWidget();?>
</div>