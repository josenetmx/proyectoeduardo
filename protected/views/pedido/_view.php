<?php
/* @var $this PedidoController */
/* @var $data Pedido */
?>

<div class="view">
    
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaDeCreacion')); ?>:</b>
	<?php echo CHtml::encode($data->fechaDeCreacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activo')); ?>:</b>
	<?php echo CHtml::encode($data->activo); ?>
	<br />


</div>