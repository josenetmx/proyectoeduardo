<?php
/* @var $this PedidoController */
/* @var $model Pedido */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pedido-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Descripcion'); ?>
		<?php echo $form->textField($model,'Descripcion',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'Descripcion'); ?>
	</div>
        <!--Lista de productos-->
        
            <?php //print_r($_POST);
            $i = 1;
            if(isset($_POST['Pedido']['idProducto'])){
                //echo "aqui";
?>
    <div class="row" id="ordenPedidos">
           
    <?php                
 foreach ($_POST['Pedido']['idProducto'] as $value) {
            echo  "<div class=\"elementos\">";
     //echo '<br />';
            echo $form->labelEx($model,'orden'); ?>
            <?php echo $form->textField($model,'orden[]',array('value'=>$i,'size'=>5,'maxlength'=>5,'id'=>'', 'class'=>'orden','disabled'=>'true')); ?>
            <?php echo $form->error($model,'orden'); ?>
            <?php if($model->isNewrecord){?>
            <?php echo $form->labelEx($model,'producto'); ?>
            <?php echo $form->textField($model,'producto[]',array('size'=>45,'maxlength'=>65,'class'=>'query','id'=>'','class'=>'query',)); ?>
            <?php echo $form->error($model,'producto'); ?>
            <?php }?>    
            <?php 
                    $productos = array('Elija uno por favor');
                    $productos = array_merge($productos,CHtml::listData(Inventario::model()->findAll(),'id','descripcion'));
                    //print_r($productos);
                    //echo $value;
                    echo $form->dropDownList($model,'idProducto[]',$productos,array('class'=>'optProducto','id'=>'','options' => array($value=>array('selected'=>true))));
                    echo $form->error($model,'idProducto');
                    //echo CHtml::dropDownList('listname', $model, array('M' => 'Male', 'F' => 'Female'));
            ?>    
            
            <?php echo $form->labelEx($model,'cantidad'); ?>
 <?php echo $form->textField($model,'cantidad[]',array('value'=>$_POST['Pedido']['cantidad'][($i-1)],'size'=>'5','maxlength'=>'5')); ?>
            <?php echo $form->error($model,'cantidad'); ?>
            
            <?php echo CHtml::button('+', array('button' => '','class'=>'addForm','name'=>'')); ?>
            <?php echo (($i>1) ? CHtml::button('-', array('button' => '','class'=>'subsForm','name'=>'')):''); ?>
            <?php  
            echo "</div>";
            $i++; } ?>            
            
        </div>
        <!--Fin lista de productos-->
<?php
}else{
?>                
 <div class = "row" id = "ordenPedidos">
    <div class = "elementos">
<?php echo $form->labelEx($model,'orden'); ?>
            <?php echo $form->textField($model,'orden[]',array('value'=>$i,'size'=>5,'maxlength'=>5,'id'=>'', 'class'=>'orden','disabled'=>'true')); ?>
            <?php echo $form->error($model,'orden'); ?>
            <?php if($model->isNewrecord){?>
            <?php echo $form->labelEx($model,'producto'); ?>
            <?php echo $form->textField($model,'producto[]',array('size'=>45,'maxlength'=>65,'class'=>'query','id'=>'','class'=>'query',)); ?>
            <?php echo $form->error($model,'producto'); ?>
            <?php }?>    
            <?php 
                    $productos = array('Elija uno por favor');
                    $productos = array_merge($productos,CHtml::listData(Inventario::model()->findAll(),'id','descripcion'));
                    //print_r($productos);
                    
                    echo $form->dropDownList($model,'idProducto[]',$productos,array('class'=>'optProducto','id'=>''));
                    echo $form->error($model,'idProducto');
                    //echo CHtml::dropDownList('listname', $model, array('M' => 'Male', 'F' => 'Female'));
            ?>    
            
            <?php echo $form->labelEx($model,'cantidad'); ?>
            <?php echo $form->textField($model,'cantidad[]',array('value'=>1,'size'=>'5','maxlength'=>'5')); ?>
            <?php echo $form->error($model,'cantidad'); ?>
            
            <?php echo CHtml::button('+', array('button' => '','class'=>'addForm','name'=>'')); ?>

    </div>
    </div>               
<?php            } 

if(!$model->isNewrecord){?>
	<div class="row">
		<?php echo $form->labelEx($model,'fechaDeCreacion'); ?>
		<?php echo $form->textField($model,'fechaDeCreacion'); ?>
		<?php echo $form->error($model,'fechaDeCreacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'activo'); ?>
		<?php echo $form->textField($model,'activo'); ?>
		<?php echo $form->error($model,'activo'); ?>
	</div>
        <?php }?>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type="text/javascript">
$(document).ready(function(){
    var arrOpc = new Array();
                <?php foreach ($productos as $key => $value) {
                echo "arrOpc[{$key}]=\"$value\";\n";
                } ?>
    var more = { 
        
        addOne : function(btn){
                //alert("Jalo");
                var orden = parseInt($(".addForm").size());
                orden++;
                //alert(orden);
                //return;
                var div_contenedor = $('#ordenPedidos');
                var div_newElement = $(document.createElement('div'));
                div_newElement.attr('class','elementos');
                //orden
                var label_1 = $(document.createElement('label'));
                label_1.attr('for','Pedido_orden');
                label_1.html('Orden');
                var input_1 = $(document.createElement('input'));
                input_1.attr({'value':orden,'size':'5','maxlength':'5','name':'Pedido[orden][]','type':'text','class':'orden','disabled':'true',});
                //buscador
                var label_2 = $(document.createElement('label'));
                label_2.attr('for','Pedido_producto');
                label_2.html('Producto');
                var input_2 = $(document.createElement('input'));
                input_2.attr({'value':'','size':'45','maxlength':'65','name':'Pedido[producto][]','type':'text','class':'query','id':''});
                input_2.keyup(function(){more.searchList(this,event);}).keydown(function( event ) {
                    if ( event.which == 13 ) {
                        event.preventDefault();
                    }
                });
                var select_1 = $(document.createElement('select'));
                select_1.attr({'name':'Pedido[idProducto][]','id':'','class':'optProducto'});
                
                var option = '';                
                $.each(arrOpc,function(i,item){
                    //console.debug(i);
                    option = $(document.createElement('option'));
                    option.attr('value',i);
                    option.html(item);
                    select_1.append(option);
                });                
                    
                //Cantidad
                var label_3 = $(document.createElement('label'));
                label_3.attr('for','Pedido_cantidad');
                label_3.html('Cantidad');
                var input_3 = $(document.createElement('input'));
                input_3.attr({'value':'1','size':'5','maxlength':'5','name':'Pedido[cantidad][]','type':'text','id':''});
                
                
                //botones mas menos
                var button_1 = $(document.createElement('button'));
                button_1.attr({'type':'button','class':'addForm','name':'','value':'+',});
                button_1.html('+');
                button_1.click(function(){more.addOne(this);});
                var button_2 = $(document.createElement('button'));
                button_2.attr({'type':'button','class':'subsForm','name':'','value':'-',});
                button_2.html('-');
                button_2.click(function(){more.subsOne(this);});
                
                div_newElement.append(label_1);
                div_newElement.append(input_1);
                div_newElement.append(label_2);
                div_newElement.append(input_2);
                div_newElement.append(select_1);
                div_newElement.append(label_3);
                div_newElement.append(input_3);
                div_newElement.append(button_1);
                div_newElement.append(button_2);
                div_contenedor.append(div_newElement);

        },
        subsOne: function(btn){
            //console.debug(Object(btn));
            var div_contenedor = $('#ordenPedidos');
            var div_elementos = $('.elementos'); 
            var num_divs = $(".subsForm").index(btn);
            num_divs++;
            //alert(num_divs);
            
            for (var i = (num_divs+1); i < div_elementos.size(); i++) {
                //var newVal = i--;
                div_elementos.eq(i).children('.orden').val(i);
                //i++
                //alert(i);
            }
            
            div_elementos.eq(num_divs).remove();
            
            //borrar div con indice
            
        },
        
        searchList:function(input,e){
            
            //console.debug(input);
            //console.debug($(input).val());
            //if(e.)
            
            
            var botones = '';
            var text = $(input);
            var cont = 0;
            
            var indice = $('.query').index(text);
            
            if($('.query').eq(indice).siblings('p').html()){
                //alert('Si');
                var sugerencia = $('.query').eq(indice).siblings('p');
               
            }else{
                //alert('No');
                var sugerencia = $(document.createElement('p'));
            }
            
            sugerencia.html('');
            sugerencia.html('Sugerencias: ');
            sugerencia.attr('data-ind',indice);
            //text.append(sugerencia);
            //return;
            if(text.val()!==''){
                $.each(arrOpc,function(i,item){
                    //console.debug(i);
                    if(i !== 0){
                        if(item.toLowerCase().indexOf(text.val()) > -1){
                            botones = $(document.createElement('button'));
                            botones.attr({'class':'sug','type':'button','val':i,});
                            botones.html(item);
                            botones.click(function(){ more.selectedId(this);});
                            botones.appendTo(sugerencia);
                            cont++;
                        }
                    }
                });
                
                if(cont==0){
                    sugerencia.html('Puede que se encuentre caduco,sin existencia en almacen o aun sin registrar')
                }
                sugerencia.insertAfter(text);
            }else{
                sugerencia.remove();
            }
            //console.debug(sugerencia);
            return;
        },
        
        selectedId : function(btn){
            var boton       = $(btn);
            var select_num  = boton.parent('p').attr('data-ind');
            $('.optProducto').eq(select_num).children("option[value|='"+boton.attr('val')+"']").attr('selected','selected');
            
            boton.parent('p').siblings('.query').val('');
            boton.parent('p').remove();
            return;
        },
        
    };
    
    $(".addForm").click(function(){more.addOne(this);});
    $(".subsForm").click(function(){more.subsOne(this);});
    $(".query").keyup(function(){more.searchList(this,event);}).keydown(function( event ) {
        if ( event.which == 13 ) {
            event.preventDefault();
        }
    });
});  
</script>    