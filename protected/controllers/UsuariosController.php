<?php

class UsuariosController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view','checkuser','refreshdiv'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        //echo "aca";
        $arr = array();
        $ocupaciones = UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us and activo',array(':fk_id_us'=>$id));
        foreach ($ocupaciones as $value) {
            array_push($arr, $value->fk_id_oc);
        }
        //$ids = implode(',', $arr);
        //echo $ids;
        //exit;
        $ocupacion = Ocupacion::model()->findAllByAttributes(array('id'=>$arr));
        //var_dump($ocupacion);
        $this->render('view', array(
            'model' => $this->loadModel($id),
            'ocupaciones' => $ocupacion,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Usuarios;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        /* print_r($_POST);
          foreach ($_POST['Ocupacion']['id'] as $key => $value) {
          echo "valor: {$value}\n";
          } */
        //exit;
        $ocupaciones = CHtml::listData(Ocupacion::model()->findAll(), 'id', 'ocupacion');
        if (isset($_POST['Usuarios'])) {
            $model->attributes = $_POST['Usuarios'];
            $model->fechaDeCreacion = date('Y-m-d H:i:s');
            $model->activo = 1;
            //print_r($model);
            //exit;
            if (!empty($model->_ocupaciones)) {
                foreach ($model->_ocupaciones as $key => $ocupacionesSeleccionadas) {
                    if (!isset($ocupaciones[$ocupacionesSeleccionadas])) {
                        unset($model->_ocupaciones[$key]);
                    }
                }
            }
            if ($model->save()) {
                //echo $model->id;
                //print_r($_POST['Ocupacion']['id']);

                foreach ($model->_ocupaciones as $key => $value) {
                    $usuarioOcupacion = new UsuariosOcupacion;
                    $usuarioOcupacion->fk_id_us = $model->id;
                    $usuarioOcupacion->fk_id_oc = $value;
                    $usuarioOcupacion->fechaDeCreacion = date('Y-m-d H:i:s');
                    $usuarioOcupacion->activo = 1;
                    //print_r($usuarioOcupacion);
                    if ($usuarioOcupacion->save()) {
                        echo "Valores: {$usuarioOcupacion->id}\n";
                    } else {
                        echo "error";
                    }
                }
                //exit;
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'ocupaciones' => $ocupaciones,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        //print_r($_POST);
        $ocupaciones = CHtml::listData(Ocupacion::model()->findAll(), 'id', 'ocupacion');


        $model->_ocupaciones = CHtml::listData(UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us', array(':fk_id_us' => $model->id)), 'fk_id_oc', 'fk_id_oc');

        if (isset($_POST['Usuarios'])) {
            $model->attributes = $_POST['Usuarios'];
            //var_dump($_POST['Ocupacion']);die;
            if (!empty($model->_ocupaciones)) {
                foreach ($model->_ocupaciones as $key => $ocupacionesSeleccionadas) {
                    if (!isset($ocupaciones[$ocupacionesSeleccionadas])) {
                        unset($model->_ocupaciones[$key]);
                    }
                }
            }
            if ($model->save()) {
                UsuariosOcupacion::model()->deleteAll('fk_id_us=:fk_id_us', array(':fk_id_us' => $model->id));
                foreach ($model->_ocupaciones as $key => $value) {
                    $usuarioOcupacion = new UsuariosOcupacion;
                    $usuarioOcupacion->fk_id_us = $model->id;
                    $usuarioOcupacion->fk_id_oc = $value;
                    $usuarioOcupacion->fechaDeCreacion = date('Y-m-d H:i:s');
                    $usuarioOcupacion->activo = 1;
                    //print_r($usuarioOcupacion);
                    if ($usuarioOcupacion->save()) {
                        echo "Valores: {$usuarioOcupacion->id}\n";
                    } else {
                        echo "error";
                    }
                }
                //$usOc = new UsuariosOcupacion;
                /* obteniendo los id seleccionados */
                /* $strIds = '';
                  foreach ($_POST['Ocupacion']['selectedID'] as $key => $value) {
                  $strIds.= "{$value},";
                  }
                  $strIds = trim($strIds, ',');
                  echo $strIds . "\n";

                  $usOc = UsuariosOcupacion::model()->findAll('fk_id_us=:fk_id_us ', array(':fk_id_us' => $model->id));


                  foreach ($usOc as $key => $value) {
                  $value->activo = 0;
                  $value->update();
                  } */

                /* foreach ($_POST['Ocupacion']['selectedID'] as $key => $value) {
                  // actualiza si existe, crea si no existe
                  $usOc = UsuariosOcupacion::model()->find('fk_id_us=:fk_id_us and fk_id_oc=:fk_id_oc', array(':fk_id_us' => $model->id, ':fk_id_oc' => $value));
                  //print_r($usOc);
                  //exit;
                  if (!empty($usOc)) {
                  $usOc->activo = 1;
                  $usOc->fechaDeCreacion = date('Y-m-d H:i:s');
                  $usOc->update();
                  } else {
                  $newUsOc = new UsuariosOcupacion();
                  $newUsOc->fk_id_us = $model->id;
                  $newUsOc->fk_id_oc = $value;
                  $newUsOc->fechaDeCreacion = date('Y-m-d H:i:s');
                  $newUsOc->activo = 1;
                  $newUsOc->save();
                  }
                  } */
                //exit;
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'ocupaciones' => $ocupaciones,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        //$dataProvider=new CActiveDataProvider('Usuarios');
        //$usuarios = Usuarios::model()->findAllBySql("SELECT u.id, u.nombre,"
//                $usuarios = Usuarios::model()->findAllBySql("SELECT u.id as id, u.nombre as nombre," 
//                    ."u.apellidos as apellidos, u.correo as correo, o.ocupacion as ocupacion, u.fechaDeCreacion as fechaDeCreacion "
//                    ."FROM usuarios u, usuariosOcupacion uO, ocupacion o "
//                    ."WHERE u.id = uO.fk_id_us AND o.id = uO.fk_id_oc "
//                    ."AND u.activo");
        $usuarios = Usuarios::model()->findAll();

        $nombres_ocupaciones_array = CHtml::listData(Ocupacion::model()->findAll(), 'id', 'ocupacion');
        $data = array();
        $data["test"]= 'recurso solicitado por controller';
        
        
        
        $this->render('index', array(
            'usuarios' => $usuarios,
            'nombres_ocupaciones_array' => $nombres_ocupaciones_array,
            'data'=>$data,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Usuarios('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Usuarios']))
            $model->attributes = $_GET['Usuarios'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Usuarios the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Usuarios::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Usuarios $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'usuarios-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    
    public function actionCheckUser(){
        $data = array();
        $data["test"]= 'recurso solicitado por ajax';
        $this->renderPartial('_confirm',array('message'=>$data),false,true);
    }
    
    public function actionRefreshDiv(){
        //print_r($_POST) ;die;
        //echo "A no m&ms";
        //exit;
    $ocupacion = Ocupacion::model()->findByPk($_POST['_ocupacion']);
        $this->renderPartial('_select',array('select'=>$ocupacion),false,true);
    }

}
