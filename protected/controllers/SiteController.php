<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
        /*
         * Action ajax 
         * 
         */
        public function actionAjaxGetImages(){
            //print_r($_POST);
            //exit;
            $url = '';
            //foreach ($_POST as $key => $value) {
                $url.= "https://api.instagram.com/v1/users/3053742249/media/recent?page=2&access_token=".$_POST['https://api_instagram_com/v1/users/3053742249/media/recent?access_token']."&";
                $url.= "&count=".$_POST['count']."&";
                $url.= "max_id&=".$_POST['max_id'];
            //    echo $key."\n";
            //}
            //$url= substr($url, 0,strlen($url)-1);
            //print_r($url);
            $arrImages = json_decode(file_get_contents($url));
            //print_r();
            $this->renderPartial('_images',array('images'=>$arrImages),false,true);
        }
        /*
         * Display the upload file page
         */
        public function actionUpload() {

        $model = new UploadFile;

        if (isset($_POST['UploadFile'])) {
            //print_r($_POST);
            //print_r($_GET);
            //print_r($_FILES);

            $model->attributes = $_POST['UploadFile'];
            $model->archivo = $_FILES['UploadFile'];
            $model->archivo = CUploadedFile::getInstance($model, 'archivo');
            //echo "Nombre del archívo: {$model->archivo['name']['archivo']}\n";
            //print_r($model->archivo);
            //unset($_FILES);
            //print_r($model);
            //exit;
            if ($model->validate()) {
                $path = realpath(Yii::app()->basePath . '/../tmp_files/');
                //echo "directorio: {$path}\n";
                //$model->archivo['name']['archivo'] = ;
                //$model->archivo['name']
                //$model->archivo->saveAs();
                $tmp_name = $path . '/' . "csv_" . time() . ".csv";
                /*$model->archivo->saveAs($tmp_name);
                
                $registros = array();
                $f = fopen($tmp_name, 'r');
                $nombres_campos = fgetcsv($f, 0, ",", "\"", "\"");
                $num_campos = count($nombres_campos);
                  
                 */
                /*

                 * 
                 * 
                 * 
                 * Registros de la cabecera
                 * 
                 *   [0] => Código
                 *   [1] => Asentamiento
                 *   [2] => Tipo
                 *   [3] => Municipio
                 *   [4] => Ciudad
                 *   [5] => Estado
                 *  
                 *                 
                 */
                $id_estado          = '';
                $str_estado         = '';
                
                $id_municipio       = '';
                $str_municipio      = '';
                
                $id_colonia         = '';
                $str_codigo         = '';
                
                $id_tipo    = '';
                $str_tipo   = '';
                
                $tipo = CHtml::listData(Asentamiento::model()->findAll(),'id','nombre');
                $estados = CHTML::listData(Estado::model()->findAll(),'id','nombre');
                $municipio = CHtml::listData(Municipio::model()->findAll(),'id','nombre');
                $codigosExitentes = CHtml::listData(Colonia::model()->findAll(),'codigo','nombre');
                print_r($codigosExitentes);
                exit;
                //echo "\n{$num_campos}";
                
                // Lee los registros
                while (($datos = fgetcsv($f, 0, ",", "\"", "\"")) !== FALSE) {
                    //echo "\nEntro";
                    // Crea un array asociativo con los nombres y valores de los campos
                    for ($icampo = 0; $icampo < $num_campos; $icampo++) {
                        $registro[$nombres_campos[$icampo]] = $datos[$icampo];
                    }
                    // Añade el registro leido al array de registros
                    $registros[] = $registro;
                    //print_r($registro);
                }
                fclose($f);
                unlink($tmp_name);
                //echo "\ncerrando";
                echo "\nLeidos " . count($registros) . " registros\n";
                //print_r($registros[1]);
                $procesados = 0;
                for ($i = 0; $i < count($registros); $i++) {
                    //$string ="\n";
                    //print_r($registros[$i]);
                    
                    //Obtener numero de estado
                    if($id_estado == '' || $str_estado != $registros[$i]['Estado']){
                        //VALIDAR SI EXISTE
                        $key_edo = array_search($registros[$i]['Estado'], $estados);
                        //$key_edo = "";
                        //echo "Llave: ".$key_edo."\n<br />";
                        //exit;
                        if(isset($estados[$key_edo])){
                         //   echo "Etapa de asignacion Estado\n<br />";
                            //exit;
                            $id_estado = $key_edo;
                            $str_estado = $estados[$id_estado];
                        }else{
                         //   echo "Etapa de guardado Estado\n<br />";
                            //exit;
                            $newEstado = new Estado;
                            $newEstado->nombre = $registros[$i]['Estado'];
                            //$newEstado->nombre = "Manuelito";
                            $newEstado->save();
                            $id_estado = $newEstado->id;
                            $str_estado = $registros[$i]['Estado'];
                            $estados = CHTML::listData(Estado::model()->findAll(),'id','nombre');
                        }
                        //echo "\n<br />Indice: ".$id_estado."\n<br />";
                    }
                    //obtener numero de municipio (Por existencia , Por nuevo registro)
                    //echo "\n<br /> $id_municipio --- $str_municipio"; 
                    if($id_municipio == '' || $str_municipio != $registros[$i]['Municipio']){
                        $key_mun = array_search($registros[$i]['Municipio'], $municipio);
                        //echo "Llave: ".$key_mun."\n<br />";
                        //$key_mun = '';
                        //if(is_null($key_mun)){ echo "\n<br /> Si";}else{  echo "\n<br /> No";}
                        if(isset($municipio[$key_mun])){
                           //echo "Etapa de asignacion Municipio\n<br />";
                           //exit;
                           $id_municipio = $key_mun;
                           $str_municipio = $municipio[$id_municipio];
                        }else{
                           //echo "Etapa de guardar Municipio\n<br />"; 
                           //exit;
                           $newMunicipio = new Municipio;
                           $newMunicipio->nombre = $registros[$i]['Municipio'];
                           $newMunicipio->save();
                           $id_municipio = $newMunicipio->id;
                           $str_municipio = $registros[$i]['Municipio'];
                           $municipio = CHtml::listData(Municipio::model()->findAll(),'id','nombre');
                        }
                        //echo "\n<br />Indice: ".$id_municipio."\n<br />";
                    }
                    //Obtener id cp (Por existencia, o por nuevo registro)
                    //obtenemos id de asentamiento
                    if($id_tipo == '' || $str_tipo != $registros[$i]['Tipo']){
                        $key_tipo = array_search($registros[$i]['Tipo'], $tipo);
                        //echo "Llave: ".$key_tipo."\n<br />";
                        if(isset($tipo[$key_tipo])){
                        //   echo "Etapa de asignacion colonia\n<br />"; 
                           $id_tipo = $key_tipo;
                           $str_tipo = $tipo[$id_tipo];
                        }else{
                        //   echo "Etapa de guardar Colonia\n<br />";  
                           $newTipo = new Asentamiento;
                           $newTipo->nombre = $registros[$i]['Tipo'];
                           $newTipo->save();
                           $id_tipo = $newTipo->id;
                           $str_tipo = $registros[$i]['Tipo'];
                           $tipo = CHtml::listData(Asentamiento::model()->findAll(),'id','nombre');
                        }
                        // echo "\n<br />Indice: ".$id_tipo."\n<br />";
                    }
                    
                    //nombre,fk_id_as,codigo
                    $colonia = Colonia::model()->find(" nombre=:nombre and fk_id_as=:fk_id_as and codigo=:codigo",array(':nombre'=>$registros[$i]['Asentamiento'],':fk_id_as'=>$id_tipo,':codigo'=>$registros[$i]["Código"],));
                    
                    if(is_null($colonia)){
                        $newColonia = new Colonia;
                        $newColonia->codigo     = $registros[$i]["Código"];
                        $newColonia->fk_id_as   = $id_tipo;
                        $newColonia->nombre     = $registros[$i]["Asentamiento"];
                        $newColonia->save();
                        $id_colonia = $newColonia->id;
                    }else{
                        $id_colonia = $colonia->id;
                    }
                    //echo "\n<br />Indice: ".$id_colonia."\n<br />";
                    
                    //Insertamos relaciones
                    //Estado_municipio
                    $edo_mun = EstadoMunicipio::model()->find("fk_id_edo=:fk_id_edo and fk_id_mun=:fk_id_mun",array(":fk_id_edo"=> $id_estado, ":fk_id_mun"=> $id_municipio));
                    if(is_null($edo_mun)){
                        $newRel_edo_mun = new EstadoMunicipio;
                        $newRel_edo_mun->fk_id_edo=$id_estado;
                        $newRel_edo_mun->fk_id_mun=$id_municipio;
                        $newRel_edo_mun->save();
                        //echo "\n<br />Indice: ".$newRel_edo_mun->id."\n<br />";
                    }
                    //Municipio_colonia
                    //fk_id_mun fk_id_col
                    $mun_col = MunicipioColonia::model()->find("fk_id_mun=:fk_id_mun and fk_id_col=:fk_id_col",array(":fk_id_col"=> $id_colonia, ":fk_id_mun"=> $id_municipio));
                    if(is_null($mun_col)){
                        $newRel_mun_col = new MunicipioColonia;
                        $newRel_mun_col->fk_id_col=$id_colonia;
                        $newRel_mun_col->fk_id_mun=$id_municipio;
                        $newRel_mun_col->save();
                        //echo "\n<br />Indice: ".$newRel_mun_col->id."\n<br />";
                    }
                    /*for ($index = 0; $index < $num_campos; $index++) {
                    $string.= $nombres_campos[$index]. " -> ".$registros[$i][$nombres_campos[$index]]." || ";    
                    
                    }
                    $string.= "\n";
                    echo $string;*/
                    $procesados++;
                    
                    //echo "\n<br> Ronda: ".$procesados;
                    //exit;
                }
                
                    echo "\nProcesados: " . $procesados . " registros\n";
                exit;
            }

            print_r($model);
            exit;
        }
        echo 'primera vez';
        //$model->isNewRecord = true;
        $this->render('uploadFile', array('model' => $model));
    }

    /**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
                        
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}